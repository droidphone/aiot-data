package net.srt.api.module.data.governance.dto.quality;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName QualityParam
 * @Author zrx
 * @Date 2023/5/28 8:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QualityParam {
	private String name;
	private String code;
}
